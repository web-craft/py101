import logging
import os
from ipaddress import IPv4Address
from typing import Literal, Optional

from pydantic import BaseModel, HttpUrl
import yaml
import aiofiles


class Config(BaseModel):
    log_level: Literal["ERROR", "WARN", "INFO", "DEBUG"] = "DEBUG"
    sentry_url: Optional[HttpUrl] = None
    port: int
    host: IPv4Address
    uvicorn_reload: bool
    e2e: bool


class ConfigMap(BaseModel):
    local: Config
    development: Config
    beta: Config
    production: Config


async def load_config() -> Config:
    async with aiofiles.open("cfg.yml", mode="rb") as file:
        contents = await file.read()
        config_map = ConfigMap(**yaml.safe_load(contents))
        environment = os.environ.get("ENV", "local")
        match environment:
            case "local":
                return config_map.local
            case "development":
                return config_map.development
            case "beta":
                return config_map.beta
            case _:
                return config_map.production


def setup_logger(level: str) -> None:
    logging.addLevelName(
        logging.ERROR,
        "\033[0;31m%s\033[1;0m"  # pylint: disable=consider-using-f-string
        % logging.getLevelName(logging.ERROR),
    )
    logging.addLevelName(
        logging.WARNING,
        "\033[0;33m%s\033[1;0m"  # pylint: disable=consider-using-f-string
        % logging.getLevelName(logging.WARNING),
    )
    logging.addLevelName(
        logging.INFO,
        "\033[0;32m%s\033[1;0m"  # pylint: disable=consider-using-f-string
        % logging.getLevelName(logging.INFO),
    )
    logging.addLevelName(
        logging.DEBUG,
        "\033[0;34m%s\033[1;0m"  # pylint: disable=consider-using-f-string
        % logging.getLevelName(logging.DEBUG),
    )
    logging.basicConfig(
        encoding="utf-8",
        format="%(levelname)s: %(message)s",
        level=level,
    )
