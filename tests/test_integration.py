from typing import Literal, Dict

import pook
import pytest
from fastapi.testclient import TestClient

from src.utils import load_config
from src.app import app


async def init(
    url: str,
    body: Dict[str, str],
    response_code: Literal["200", "404"] = "200",
    method: Literal["get", "post"] = "get",
) -> TestClient:
    config = await load_config()
    if not config.e2e:
        pook.on()
        getattr(pook, method)(url, reply=response_code, response_json=body)
    return TestClient(app)


def describe_the_rest_api() -> None:
    @pytest.mark.asyncio
    async def it_can_get_a_url() -> None:
        client = await init(
            "https://httpbin.org/get", {"url": "https://httpbin.org/get"}
        )
        response = client.get("/")
        assert response.text.strip('"') == "https://httpbin.org/get"
