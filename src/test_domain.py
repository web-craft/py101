import pytest

from src.domain import adder


def describe_the_domain() -> None:
    @pytest.mark.asyncio
    def it_adds() -> None:
        actual = adder(1, 2)
        assert actual == 3
