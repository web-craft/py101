# [py] 🐍

![](https://img.shields.io/gitlab/pipeline-status/web-craft/py101?branch=development&logo=gitlab&style=for-the-badge)
![](https://img.shields.io/gitlab/coverage/web-craft/py101/development?logo=pytest&style=for-the-badge)
