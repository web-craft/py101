import asyncio
import logging

import sentry_sdk
import uvicorn

from src.utils import load_config, setup_logger


async def main() -> None:
    config = await load_config()
    sentry_sdk.init(config.sentry_url)
    setup_logger(config.log_level)
    if __name__ == "__main__":
        uvicorn.run(
            "src.lib:app",
            host=str(config.host),
            port=config.port,
            reload=config.uvicorn_reload,
            log_level="error",
        )
    logging.info(f"Running with: Config( {config} )")


asyncio.run(main())
