from aiohttp import ClientSession
from fastapi import FastAPI

app = FastAPI()


@app.get("/")
async def read_main() -> str:
    session = ClientSession()
    async with session.get("https://httpbin.org/get") as response:
        data = await response.json()
    await session.close()
    return str(data["url"])
